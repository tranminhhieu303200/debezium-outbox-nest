import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { Comment, CommentStatus } from '../domain/comment.entity';
import { EntityRepository } from '@mikro-orm/mysql';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CommentApprovedEvent } from '../domain/comment-approved.event';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepo: EntityRepository<Comment>,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async approveComment(id: number): Promise<number> {
    const comment = await this.commentRepo.findOne(id);
    if (!comment) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          msg: 'Not found',
          comment_id: id,
        },
        HttpStatus.NOT_FOUND,
      );
    }

    comment.status = CommentStatus.APPROVED;
    comment.approvedAt = new Date();

    this.commentRepo.persist(comment);
    this.eventEmitter.emit(
      'outbox.event.Comment',
      new CommentApprovedEvent(
        comment.id,
        comment as unknown as Record<string, unknown>,
      ),
    );

    return comment.id;
  }
}
