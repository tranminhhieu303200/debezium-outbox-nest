import { InjectRepository } from '@mikro-orm/nestjs';
import { Film } from '../domain/film.entity';
import { EntityRepository } from '@mikro-orm/mysql';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { FilmCreatedEvent } from '../domain/film-created.event';
import { CreateFilmDto } from '../dto/create-film.dto';

@Injectable()
export class FilmService {
  constructor(
    @InjectRepository(Film) private readonly filmRepo: EntityRepository<Film>,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  createFilm(createFilmDto: CreateFilmDto): Film {
    const film = new Film();
    film.name = createFilmDto.name;
    film.description = createFilmDto.description;

    this.filmRepo.persist(film);
    this.eventEmitter.emit(
      'outbox.event.Film',
      new FilmCreatedEvent(film.id, film as unknown as Record<string, unknown>),
    );

    return film;
  }
}
