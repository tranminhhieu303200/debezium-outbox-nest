import { Controller, Param, Patch } from '@nestjs/common';
import { CommentService } from '../service/comment.service';

@Controller('comments')
export class CommentResource {
  constructor(private readonly commentService: CommentService) {}

  @Patch(':id/approve')
  async approveComment(@Param('id') id: number): Promise<number> {
    return this.commentService.approveComment(id);
  }
}
