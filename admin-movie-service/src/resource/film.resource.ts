import { Body, Controller, Post } from '@nestjs/common';
import { FilmService } from '../service/film.service';
import { Film } from '../domain/film.entity';
import { CreateFilmDto } from '../dto/create-film.dto';

@Controller('films')
export class FilmResource {
  constructor(private readonly filmService: FilmService) {}

  @Post()
  createFilm(@Body() createFilmDto: CreateFilmDto): Film {
    return this.filmService.createFilm(createFilmDto);
  }
}
