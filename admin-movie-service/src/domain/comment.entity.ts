import { Entity, PrimaryKey, Property } from '@mikro-orm/core';

export enum CommentStatus {
  PENDING,
  APPROVED,
  REJECTED,
}

@Entity({ tableName: 'comments' })
export class Comment {
  @PrimaryKey()
  id: number;

  @Property()
  comment!: string;

  @Property({ fieldName: 'film_id' })
  filmId!: number;

  @Property({ fieldName: 'user_display_name' })
  userDisplayName!: string;

  @Property({ fieldName: 'user_email' })
  userEmail!: string;

  @Property()
  status!: CommentStatus;

  @Property({ fieldName: 'approved_at', nullable: true })
  approvedAt: Date;

  @Property({ fieldName: 'created_at' })
  createdAt: Date;

  @Property({ fieldName: 'updated_at', onUpdate: () => new Date() })
  updatedAt: Date;
}
