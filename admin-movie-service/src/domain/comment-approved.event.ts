import { DomainEvent } from '@htc/nest-outbox-mikroorm';

export class CommentApprovedEvent extends DomainEvent {
  constructor(id: number, comment: Record<string, unknown>) {
    super('Comment', id.toString(), 'CommentApproved', comment, Date.now());
  }
}
