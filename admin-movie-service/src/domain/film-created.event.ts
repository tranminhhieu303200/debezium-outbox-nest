import { DomainEvent } from '@htc/nest-outbox-mikroorm';

export class FilmCreatedEvent extends DomainEvent {
  constructor(private id: number, private film: Record<string, unknown>) {
    super('Film', id.toString(), 'FilmCreated', film, Date.now());
  }
}
