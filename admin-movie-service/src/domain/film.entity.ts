import { Entity, PrimaryKey, Property } from '@mikro-orm/core';
import { snowflake } from '../utils/snowflake.util';

@Entity({ tableName: 'films' })
export class Film {
  @PrimaryKey()
  id: number = +snowflake.generate();

  @Property()
  name!: string;

  @Property()
  description?: string;

  @Property({ fieldName: 'created_at' })
  createdAt = new Date();

  @Property({ fieldName: 'updated_at', onUpdate: () => new Date() })
  updatedAt = new Date();
}
