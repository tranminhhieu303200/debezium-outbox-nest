import { Module } from '@nestjs/common';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { EventEmitterModule } from '@nestjs/event-emitter';
import {
  EventDispatcher,
  OutboxEvent,
  ReceivedMessage,
  SqlDuplicateDetector,
} from '@htc/nest-outbox-mikroorm';
import { FilmService } from './service/film.service';
import { FilmResource } from './resource/film.resource';
import { Film } from './domain/film.entity';
import { ConfigModule } from '@nestjs/config';
import { KafkaModule } from '@rob3000/nestjs-kafka';
import { CommentEventHandler } from './messaging/comment-event.handler';
import { Comment } from './domain/comment.entity';
import { CommentService } from './service/comment.service';
import { CommentResource } from './resource/comment.resource';

@Module({
  imports: [
    ConfigModule.forRoot(),
    EventEmitterModule.forRoot({
      wildcard: true,
    }),
    KafkaModule.register([
      {
        name: 'ADMIN_MOVIE_SERVICE',
        options: {
          client: {
            clientId: 'admin-movie',
            brokers: process.env.KAFKA_BROKERS.split(','),
            ssl: !!process.env.KAFKA_SECURITY,
            sasl: !!process.env.KAFKA_SECURITY
              ? {
                  mechanism: 'plain',
                  username: process.env.KAFKA_API_KEY,
                  password: process.env.KAFKA_API_SECRET,
                }
              : undefined,
          },
          consumer: {
            groupId: 'admin-service',
          },
        },
      },
    ]),
    MikroOrmModule.forRoot({
      entities: ['../dist/**/*.entity.js'],
      entitiesTs: ['../src/**/*.entity.ts'],
      dbName: 'movie',
      type: 'mysql',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      autoLoadEntities: true,
      debug: true,
      driverOptions: {
        connection: { ssl: { rejectUnauthorized: false } },
      },
      allowGlobalContext: true,
    }),
    MikroOrmModule.forFeature({
      entities: [OutboxEvent, ReceivedMessage, Film, Comment],
    }),
  ],
  controllers: [FilmResource, CommentResource],
  providers: [
    EventDispatcher,
    SqlDuplicateDetector,
    FilmService,
    CommentEventHandler,
    CommentService,
  ],
})
export class AppModule {}
