import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { IHeaders, KafkaService, SubscribeTo } from '@rob3000/nestjs-kafka';
import { SqlDuplicateDetector } from '@htc/nest-outbox-mikroorm';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/mysql';
import { Comment, CommentStatus } from '../domain/comment.entity';

@Injectable()
export class CommentEventHandler implements OnModuleInit {
  private readonly logger = new Logger(CommentEventHandler.name);

  constructor(
    @Inject('ADMIN_MOVIE_SERVICE') private readonly client: KafkaService,
    private readonly dupDetector: SqlDuplicateDetector,
    @InjectRepository(Comment)
    private readonly commentRepo: EntityRepository<Comment>,
  ) {}

  onModuleInit() {
    this.client.subscribeToResponseOf('outbox.event.Comment', this);
  }

  @SubscribeTo('outbox.event.Comment')
  async handle(
    data: any,
    key: any,
    offset: number,
    timestamp: number,
    partition: number,
    headers: IHeaders,
  ): Promise<void> {
    this.logger.log(
      `Received message: ${data}, at ${timestamp}
      with headers: ${JSON.stringify(headers)}`,
    );

    const messageId = headers['id'];
    const eventType = headers['eventType'];

    switch (eventType) {
      case 'CommentCreated':
        await this.dupDetector.doWithMessage(
          'admin-service',
          messageId,
          async () => {
            await this.handleCommentCreated(
              messageId,
              JSON.parse(data).payload as Record<string, unknown>,
            );
          },
        );
        break;
      default:
    }
  }

  private async handleCommentCreated(
    _messageId: string,
    _data: Record<string, unknown>,
  ): Promise<void> {
    const comment = new Comment();
    comment.id = _data.commentId as number;
    comment.comment = _data.comment as string;
    comment.filmId = _data.filmId as number;
    comment.status = _data.status as CommentStatus;
    comment.userDisplayName = _data.userDisplayName as string;
    comment.userEmail = _data.userEmail as string;
    comment.createdAt = new Date(_data.createdAt as number);
    comment.updatedAt = new Date(_data.updatedAt as number);

    try {
      await this.commentRepo.persistAndFlush(comment);
    } catch (e) {
      this.logger.error(e);
      this.logger.log(`Message: ${_messageId}`);
    }
  }
}
