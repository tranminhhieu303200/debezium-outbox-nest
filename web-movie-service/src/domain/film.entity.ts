import {
  Entity,
  PrimaryKey,
  Property,
  SerializedPrimaryKey,
  Unique,
} from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';
import { MongoBaseEntity } from '@htc/nest-outbox-mikroorm';

@Entity({ tableName: 'films' })
export class Film extends MongoBaseEntity {
  @PrimaryKey()
  _id: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @PrimaryKey({ fieldName: 'film_id' })
  @Unique()
  filmId!: string;

  @Property()
  name!: string;

  @Property()
  description?: string;

  @Property({ fieldName: 'created_at' })
  createdAt = new Date();

  @Property({ fieldName: 'updated_at', onUpdate: () => new Date() })
  updatedAt = new Date();
}
