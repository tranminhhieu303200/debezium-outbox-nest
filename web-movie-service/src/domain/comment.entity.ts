import {
  Entity,
  PrimaryKey,
  Property,
  SerializedPrimaryKey,
  Unique,
} from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';
import { snowflake } from '../utils/snowflake.util';
import { MongoBaseEntity } from '@htc/nest-outbox-mikroorm';

export enum CommentStatus {
  PENDING,
  APPROVED,
  REJECTED,
}

@Entity({ tableName: 'comments' })
export class Comment extends MongoBaseEntity {
  @PrimaryKey()
  _id: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @PrimaryKey({ fieldName: 'comment_id' })
  @Unique()
  commentId = snowflake.generate();

  @Property()
  comment!: string;

  @Property({ fieldName: 'film_id' })
  filmId!: string;

  @Property({ fieldName: 'user_display_name' })
  userDisplayName!: string;

  @Property({ fieldName: 'user_email' })
  userEmail!: string;

  @Property()
  status = CommentStatus.PENDING;

  @Property({ fieldName: 'approved_at', nullable: true })
  approvedAt: Date;

  @Property({ fieldName: 'created_at' })
  createdAt = new Date();

  @Property({ fieldName: 'updated_at', onUpdate: () => new Date() })
  updatedAt = new Date();
}
