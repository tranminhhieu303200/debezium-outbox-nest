import { DomainEvent } from '@htc/nest-outbox-mikroorm';

export class CommentCreatedEvent extends DomainEvent {
  constructor(id: string, comment: Record<string, unknown>) {
    super('Comment', id, 'CommentCreated', comment, Date.now());
  }
}
