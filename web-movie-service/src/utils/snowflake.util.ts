import { Snowflake } from '@htc/common-libs';

export const snowflake = new Snowflake({
  mid: 2,
  offset: (2022 - 1970) * 31536000 * 1000,
});
