export class CreateCommentDto {
  comment!: string;
  filmId!: string;
  userDisplayName!: string;
  userEmail!: string;
}
