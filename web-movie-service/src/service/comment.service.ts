import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/mongodb';
import { Comment } from '../domain/comment.entity';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CreateCommentDto } from '../dto/create-comment.dto';
import { CommentCreatedEvent } from '../domain/comment-created.event';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepo: EntityRepository<Comment>,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async createComment(createCommentDto: CreateCommentDto): Promise<Comment> {
    const comment = new Comment();
    comment.comment = createCommentDto.comment;
    comment.filmId = createCommentDto.filmId;
    comment.userDisplayName = createCommentDto.userDisplayName;
    comment.userEmail = createCommentDto.userEmail;

    this.commentRepo.persist(comment);
    this.eventEmitter.emit(
      'outbox.event.Comment',
      new CommentCreatedEvent(
        comment.commentId,
        comment as unknown as Record<string, unknown>,
      ),
    );

    return comment;
  }
}
