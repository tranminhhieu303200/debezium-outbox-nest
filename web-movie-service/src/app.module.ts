import { Module } from '@nestjs/common';
import { FilmEventHandler } from './messaging/film-event.handler';
import { KafkaModule } from '@rob3000/nestjs-kafka';
import { ConfigModule } from '@nestjs/config';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Film } from './domain/film.entity';
import {
  MongoEventDispatcher,
  MongoOutboxEvent,
} from '@htc/nest-outbox-mikroorm';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { CommentService } from './service/comment.service';
import { Comment } from './domain/comment.entity';
import { CommentResource } from './resource/comment.resource';
import { CommentEventHandler } from './messaging/comment-event.handler';

@Module({
  imports: [
    ConfigModule.forRoot(),
    EventEmitterModule.forRoot({
      wildcard: true,
    }),
    KafkaModule.register([
      {
        name: 'WEB_MOVIE_SERVICE',
        options: {
          client: {
            clientId: 'web-movie',
            brokers: process.env.KAFKA_BROKERS.split(','),
            ssl: !!process.env.KAFKA_SECURITY,
            sasl: !!process.env.KAFKA_SECURITY
              ? {
                  mechanism: 'plain',
                  username: process.env.KAFKA_API_KEY,
                  password: process.env.KAFKA_API_SECRET,
                }
              : undefined,
          },
          consumer: {
            groupId: 'web-service',
          },
        },
      },
    ]),
    MikroOrmModule.forRoot({
      entities: ['../dist/**/*.entity.js'],
      entitiesTs: ['../src/**/*.entity.ts'],
      dbName: 'movie',
      type: 'mongo',
      clientUrl: process.env.MONGO_URL,
      ensureIndexes: true,
      autoLoadEntities: true,
      debug: true,
      allowGlobalContext: true,
      implicitTransactions: true,
    }),
    MikroOrmModule.forFeature({
      entities: [MongoOutboxEvent, Film, Comment],
    }),
  ],
  controllers: [CommentResource],
  providers: [
    MongoEventDispatcher,
    FilmEventHandler,
    CommentService,
    CommentEventHandler,
  ],
})
export class AppModule {}
