import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { IHeaders, KafkaService, SubscribeTo } from '@rob3000/nestjs-kafka';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/mongodb';
import { Comment, CommentStatus } from '../domain/comment.entity';

@Injectable()
export class CommentEventHandler implements OnModuleInit {
  private readonly logger = new Logger(CommentEventHandler.name);

  constructor(
    @Inject('WEB_MOVIE_SERVICE') private readonly client: KafkaService,
    @InjectRepository(Comment)
    private readonly commentRepo: EntityRepository<Comment>,
  ) {}

  onModuleInit() {
    this.client.subscribeToResponseOf('outbox.event.Comment', this);
  }

  @SubscribeTo('outbox.event.Comment')
  async handle(
    data: any,
    key: any,
    offset: number,
    timestamp: number,
    partition: number,
    headers: IHeaders,
  ): Promise<void> {
    this.logger.log(
      `Received message: ${data}, at ${timestamp}
      with headers: ${JSON.stringify(headers)}`,
    );

    const messageId = headers['id'];
    const eventType = headers['eventType'];

    switch (eventType) {
      case 'CommentApproved':
        await this.handleCommentApproved(
          messageId,
          JSON.parse(data).payload as Record<string, unknown>,
        );
        break;
      default:
    }
  }

  private async handleCommentApproved(
    _messageId: string,
    _data: Record<string, unknown>,
  ): Promise<void> {
    const commentId = _data.id.toString();
    const comment = await this.commentRepo.findOne({ commentId });

    if (comment) {
      if (comment._messageIds.indexOf(_messageId) !== -1) {
        this.logger.error(`Message: ${_messageId}`);
        return;
      }

      comment.status = _data.status as CommentStatus;
      comment.approvedAt = _data.approvedAt as Date;
      comment._messageIds.push(_messageId);

      try {
        await this.commentRepo.persistAndFlush(comment);
      } catch (e) {
        this.logger.error(e);
        this.logger.log(`Message: ${_messageId}`);
      }
    }
  }
}
