import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { IHeaders, KafkaService, SubscribeTo } from '@rob3000/nestjs-kafka';
import { InjectRepository } from '@mikro-orm/nestjs';
import { Film } from '../domain/film.entity';
import { MongoEntityRepository } from '@mikro-orm/mongodb';

@Injectable()
export class FilmEventHandler implements OnModuleInit {
  private readonly logger = new Logger(FilmEventHandler.name);

  constructor(
    @Inject('WEB_MOVIE_SERVICE') private readonly client: KafkaService,
    @InjectRepository(Film)
    private readonly filmRepo: MongoEntityRepository<Film>,
  ) {}

  onModuleInit() {
    this.client.subscribeToResponseOf('outbox.event.Film', this);
  }

  @SubscribeTo('outbox.event.Film')
  async handle(
    data: any,
    key: any,
    offset: number,
    timestamp: number,
    partition: number,
    headers: IHeaders,
  ): Promise<void> {
    this.logger.log(
      `Received message: ${data}, at ${timestamp}
      with headers: ${JSON.stringify(headers)}`,
    );
    const messageId = headers['id'];
    const eventType = headers['eventType'];

    switch (eventType) {
      case 'FilmCreated':
        await this.handleFilmCreated(
          messageId,
          JSON.parse(data).payload as Record<string, unknown>,
        );
        break;
      default:
    }
  }

  private async handleFilmCreated(
    _messageId: string,
    _data: Record<string, unknown>,
  ): Promise<void> {
    const film = new Film();
    film.filmId = _data.id as string;
    film.name = _data.name as string;
    film.description = _data.description ? (_data.description as string) : null;
    film._messageIds.push(_messageId);

    try {
      await this.filmRepo.persistAndFlush(film);
    } catch (e) {
      this.logger.error(e);
      this.logger.log(`Message: ${_messageId}`);
    }
  }
}
