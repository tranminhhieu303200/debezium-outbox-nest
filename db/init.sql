USE movie;

CREATE TABLE IF NOT EXISTS `films` (
    `id` BIGINT NOT NULL PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) DEFAULT NULL,
    `created_at` DATETIME NOT NULL,
    `updated_at` DATETIME NOT NULL
);

CREATE TABLE IF NOT EXISTS `comments` (
    `id` BIGINT NOT NULL PRIMARY KEY,
    `comment` VARCHAR(255) NOT NULL,
    `film_id` BIGINT NOT NULL,
    `user_display_name` VARCHAR(255),
    `user_email` VARCHAR(255),
    `status` TINYINT,
    `approved_at` DATETIME DEFAULT NULL,
    `created_at` DATETIME NOT NULL,
    `updated_at` DATETIME NOT NULL
);

CREATE TABLE IF NOT EXISTS `outboxevent` (
    `id` VARCHAR(36) NOT NULL PRIMARY KEY,
    `aggregate_type` VARCHAR(255) NOT NULL,
    `aggregate_id` VARCHAR(18) NOT NULL,
    `event_type` VARCHAR(255) NOT NULL,
    `payload` JSON NOT NULL,
    `timestamp` BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS `received_messages` (
    `consumer_id` VARCHAR(255) NOT NULL,
    `message_id` VARCHAR(255) NOT NULL,
    `created_at` DATETIME NOT NULL,
    PRIMARY KEY(consumer_id, message_id)
);
