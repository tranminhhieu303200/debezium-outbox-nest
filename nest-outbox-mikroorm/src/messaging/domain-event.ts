export class DomainEvent {
  constructor(
    private aggregateType: string,
    private aggregateId: string,
    private eventType: string,
    private payload: Record<string, unknown>,
    private timestamp: number,
  ) {}

  getAggregateType(): string {
    return this.aggregateType;
  }

  getAggregateId(): string {
    return this.aggregateId;
  }

  getEventType(): string {
    return this.eventType;
  }

  getPayload(): Record<string, unknown> {
    return this.payload;
  }

  getTimestamp(): number {
    return this.timestamp;
  }
}
