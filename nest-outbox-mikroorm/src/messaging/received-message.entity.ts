import { Entity, PrimaryKey, Property, Unique } from '@mikro-orm/core';

@Entity({ tableName: 'received_messages' })
export class ReceivedMessage {
  @PrimaryKey({ fieldName: 'consumer_id' })
  consumerId: string;

  @PrimaryKey({ fieldName: 'message_id' })
  @Unique()
  messageId: string;

  @Property({ fieldName: 'created_at' })
  createdAt = new Date();
}
