import { Injectable, Logger } from '@nestjs/common';
import { EntityManager } from '@mikro-orm/core';
import { ReceivedMessage } from './received-message.entity';

@Injectable()
export class SqlDuplicateDetector {
  private readonly logger = new Logger(SqlDuplicateDetector.name);

  constructor(private readonly em: EntityManager) {}

  async doWithMessage(
    consumerId: string,
    messageId: string,
    cb: () => Promise<void>,
  ): Promise<void> {
    const checkDuplicate = await this.isDuplicate(consumerId, messageId);

    if (!checkDuplicate) {
      await cb();
    }
  }

  private async isDuplicate(
    _consumerId: string,
    _messageId: string,
  ): Promise<boolean> {
    try {
      const msg = new ReceivedMessage();
      msg.consumerId = _consumerId;
      msg.messageId = _messageId;

      await this.em.persistAndFlush(msg);

      return false;
    } catch (e) {
      this.logger.error(e);
      this.logger.log(`Message: ${_messageId}`);

      return true;
    }
  }
}
