import { EntityManager } from '@mikro-orm/core';
import { DomainEvent } from './domain-event';
import { OutboxEvent } from './outbox-event.entity';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

@Injectable()
export class EventDispatcher {
  constructor(private readonly em: EntityManager) {}

  @OnEvent('outbox.event.*')
  async onDomainEvent(event: DomainEvent): Promise<void> {
    const outboxEvent = new OutboxEvent();
    outboxEvent.aggregateId = event.getAggregateId();
    outboxEvent.aggregateType = event.getAggregateType();
    outboxEvent.eventType = event.getEventType();
    outboxEvent.payload = event.getPayload();
    outboxEvent.timestamp = event.getTimestamp();

    await this.em.persistAndFlush(outboxEvent);
    await this.em.removeAndFlush(outboxEvent);
  }
}
