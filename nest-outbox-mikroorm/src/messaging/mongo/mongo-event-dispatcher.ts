import { EntityManager } from '@mikro-orm/core';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { MongoOutboxEvent } from './mongo-outbox-event.entity';
import { DomainEvent } from '../domain-event';

@Injectable()
export class MongoEventDispatcher {
  constructor(private readonly em: EntityManager) {}

  @OnEvent('outbox.event.*')
  async onDomainEvent(event: DomainEvent): Promise<void> {
    const outboxEvent = new MongoOutboxEvent();
    outboxEvent.aggregateId = event.getAggregateId();
    outboxEvent.aggregateType = event.getAggregateType();
    outboxEvent.eventType = event.getEventType();
    outboxEvent.payload = event.getPayload();
    outboxEvent.timestamp = event.getTimestamp();

    await this.em.persistAndFlush(outboxEvent);
    await this.em.removeAndFlush(outboxEvent);
  }
}
