import {
  Entity,
  PrimaryKey,
  Property,
  SerializedPrimaryKey,
} from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';

@Entity({ tableName: 'outboxevent' })
export class MongoOutboxEvent {
  @PrimaryKey()
  _id: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @Property({ fieldName: 'aggregate_type' })
  aggregateType!: string;

  @Property({ fieldName: 'aggregate_id' })
  aggregateId!: string;

  @Property({ fieldName: 'event_type' })
  eventType!: string;

  @Property()
  timestamp!: number;

  @Property({ type: 'json' })
  payload!: Record<string, unknown>;
}
