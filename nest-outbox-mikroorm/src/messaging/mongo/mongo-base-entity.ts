import { Property } from '@mikro-orm/core';

export abstract class MongoBaseEntity {
  @Property({ fieldName: 'message_ids', hidden: true, type: 'array' })
  _messageIds: string[] = [];
}
