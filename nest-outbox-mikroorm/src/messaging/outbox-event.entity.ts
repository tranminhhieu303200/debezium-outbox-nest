import { Entity, PrimaryKey, Property } from '@mikro-orm/core';
import { v4 } from 'uuid';

@Entity({ tableName: 'outboxevent' })
export class OutboxEvent {
  @PrimaryKey()
  id: string = v4();

  @Property({ fieldName: 'aggregate_type' })
  aggregateType!: string;

  @Property({ fieldName: 'aggregate_id' })
  aggregateId!: string;

  @Property({ fieldName: 'event_type' })
  eventType!: string;

  @Property()
  timestamp!: number;

  @Property({ type: 'json' })
  payload!: Record<string, unknown>;
}
