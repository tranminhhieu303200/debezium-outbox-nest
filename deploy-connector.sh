#!/bin/sh
CONNECT_ENDPOINT=$1
FILE=$2

if [ -z "$CONNECT_ENDPOINT" ] || [ -z "$FILE" ] ; then
  echo "Must provide both endpoint and file!"
  exit 1
fi

curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" "$CONNECT_ENDPOINT" -d "@$FILE"

